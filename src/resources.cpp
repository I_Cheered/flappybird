#include <iostream>
#include "resources.hpp"

bool collission(vec2d birdpos, double birdsize, double pipewidth, double gapheight, vec2d pipe1pos, vec2d pipe2pos)
{
    if(((birdpos.x + birdsize) > pipe1pos.x) && (birdpos.x < (pipe1pos.x + pipewidth)))
    {
        if((birdpos.y < pipe1pos.y) || ((birdpos.y + birdsize) > (pipe1pos.y + gapheight)))
        {
            return true;
        }
    }
        
    else if(((birdpos.x + birdsize) > pipe2pos.x) && (birdpos.x < (pipe2pos.x + pipewidth)))
    {
        if((birdpos.y < pipe2pos.y) || ((birdpos.y + birdsize) > (pipe2pos.y + gapheight)))
        {
            return true;            
        }
    }
    return false;
}
/*
void restart()
{
    Obird.pos.x = windowWidth / 2 - Obird.size;
    Obird.pos.y = windowHeight / 2 - Obird.size / 2;
    Obird.velocity = 0;
    Opipe.pipepos.x = Opipe.gappos.x = windowWidth;
    Opipe.gappos.y = Opipe.gappos.y = rand() % (windowHeight - 300);
    Opipe2.pipepos.x = Opipe2.gappos.x = 1.5*windowWidth + Opipe2.width/2;
    Opipe2.gappos.y = Opipe2.gappos.y = rand() % (windowHeight - 300);
    Obird.move();
    Opipe.move();
    Opipe2.move();
}*/