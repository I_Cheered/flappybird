#include <iostream>
#include "bird.hpp"

Bird::Bird()
{
    size = 75;
    gravity = 10;
    jumpvelocity = 3;
    velocity = 0; 
    
    bird = sf::RectangleShape(sf::Vector2f(size, size));
    bird.setFillColor(sf::Color::Yellow);

    restart();
}

void Bird::restart()
{
    pos.x = windowWidth / 2 - size / 2;
    pos.y = windowHeight / 2 - size / 2;
    bird.setPosition(pos.x, pos.y);
}

void Bird::move(double deltaT)
{
    pos.y = pos.y - velocity * deltaT;
    if(pos.y < 0)
    {
        pos.y = 0; 
        velocity = 0;
    }
    if(pos.y > windowHeight - size)
        pos.y = windowHeight - size;
    bird.setPosition(pos.x, pos.y);
    velocity = velocity - gravity * 0.01;
}

void Bird::jump()
{
    velocity = jumpvelocity;
}

void Bird::draw()
{
    window.draw(bird);
}
