#include "pipe.hpp"

Pipe::Pipe(double pos)
{   
    startpos = pos;

    gapsize = 300;
    width = 100;

    pipe = sf::RectangleShape(sf::Vector2f(width, windowHeight));
    gap = sf::RectangleShape(sf::Vector2f(width, gapsize));

    pipe.setFillColor(sf::Color::Green);
    gap.setFillColor(sf::Color::Black);

    restart();
}

void Pipe::restart()
{
    pos.x = windowWidth + startpos * 0.5 * (windowWidth + width);
    pos.y = rand() % (int)((windowHeight - gapsize));
    pipe.setPosition(pos.x,0);
    gap.setPosition(pos.x, pos.y);
}

void Pipe::move(double deltaT)
{
    pos.x = pos.x - 1 * deltaT;
    if(pos.x < (-width))
        pos.x = windowWidth;
    pipe.setPosition(pos.x, 0);
    gap.setPosition(pos.x, pos.y);
}

void Pipe::draw()
{
    window.draw(pipe);
    window.draw(gap);
}