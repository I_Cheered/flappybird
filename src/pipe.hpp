#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "resources.hpp"

extern const double windowHeight;
extern const double windowWidth;
extern sf::RenderWindow window;

class Pipe
{
private:
    sf::RectangleShape pipe, gap;
    double startpos;
public:
    double gapsize, width;
    vec2d pos;

    Pipe(double pos);
    void move(double deltaT);
    void draw();
    void restart();
};
