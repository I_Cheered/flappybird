#include <iostream>
#include <cmath>
#include <chrono>
#include <stdlib.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "resources.hpp"

#include "bird.hpp"
#include "pipe.hpp"

extern const double windowHeight = 800;
extern const double windowWidth = 1200;
extern sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "sfml_window");

int main()
{
    double deltaT;
    int score;

    gameState gamestate = start;
    score = 0;
    deltaT = 0;

    std::chrono::high_resolution_clock clock;
    std::chrono::high_resolution_clock::time_point t0 , t1;
    std::chrono::duration<double> looptime;
    srand(time(NULL));
    sf::Event event;

    Bird bird; 
    Pipe pipe1(0);
    Pipe pipe2(1);

    //////////////////////////////////////////////
    int counter = 0;
    while (window.isOpen()) //START GAMELOOP
    {
        t0 = clock.now();

        while (window.pollEvent(event))
            if (event.type == sf::Event::Closed)
                window.close();
        
        if(gamestate == start)
        {
            gamestate = play;
        }

        if(gamestate == play)
        {
            if(collission(bird.pos, bird.size, pipe1.width, pipe1.gapsize, pipe1.pos, pipe2.pos))
            {
                gamestate = dead;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
                bird.jump();

            bird.move(deltaT);
            pipe1.move(deltaT);
            pipe2.move(deltaT);

            window.clear(sf::Color::Blue);

            
            pipe1.draw();
            pipe2.draw();
            bird.draw();

            window.display();
        }
        if(gamestate == dead)
        {
            

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            {
                bird.restart();
                pipe1.restart();
                pipe2.restart();

                gamestate = play;
            } 
        }
        t1 = clock.now();
        looptime = std::chrono::duration_cast<std::chrono::duration<double>>(t1 - t0);
        deltaT = 1*(looptime.count() / 0.00436166);
    }   //END GAMELOOPS
    //////////////////////////////////////////////
}