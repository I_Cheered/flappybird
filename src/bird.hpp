#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "resources.hpp"

extern const double windowHeight;
extern const double windowWidth;
extern sf::RenderWindow window;

class Bird
{
public:
    //sf::Texture texture;
    //sf::Sprite bird;

    double size, gravity, jumpvelocity, velocity;
    vec2d pos;
    sf::RectangleShape bird;

    Bird(); 
    void move(double deltaT);
    void jump();
    void draw();
    void restart();
};