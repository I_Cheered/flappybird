@echo off
echo Preparing files for debugging...
echo Following warnings were found: 
g++ -g -o dependencies/SFML/bin/run -Idependencies/SFML/include src/*.cpp -Ldependencies/SFML/lib -lsfml-graphics -lsfml-window -lsfml-system
echo Done, starting debugger.